#ifndef ENTETE_H_INCLUDED
#define ENTETE_H_INCLUDED

#define TAILLE 20

    void GetTxt(FILE*);
    void Saisi(char *,char *,char *);
    void Cryptage(char *,char *,char *);
    char Encryptage(char,char);
    char Decryptage(char,char);
    void SaisiCryptage(char*);
    void SaisiPerroquet(char*);
    void SaisiNom(char*);

#endif // ENTETE_H_INCLUDED
