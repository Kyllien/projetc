#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "entete.h"


void Saisi(char *nom,char *perroquet, char *crypt)
{

    SaisiNom(nom);

    SaisiPerroquet(perroquet);

    SaisiCryptage(crypt);
}

//Fonction pour get le nom du fichier source et verifier si celui ci existe
void SaisiNom(char* nom)
{
    //Declaration
    FILE* f_source=NULL;

    while(f_source==NULL)
    {
        printf("Quel est le nom du fichier ?\n");
        fflush(stdin);
        gets(nom);
        f_source = fopen(nom,"r");
        if(f_source==NULL)
        {
            printf("Pas le bon nom de fichier\n");
        }
        fclose(f_source);
    }
}


//Fonction pour la saisie du perroquet
void SaisiPerroquet(char *perroquet)
{
    // Declaration
    FILE* f_pero=NULL;
    f_pero = fopen("peroq.def","a+");
    char chaine[TAILLE]="";
    int compteur=1;
    int com=0;

    printf("\nQuel est le perroquet a utiliser ?\n");
    if(f_pero!=NULL)
    {
        while(fgets(chaine, TAILLE, f_pero) != NULL)
        {
            //Gestion des cl�s avec une grande chaine de caract�res en tant que perroquet
            if(chaine[strlen(chaine)-1]=='\n')
            {
                //Gestion erreur ou chaine ne contient que '\n'
                if(chaine[0]!='\n'){
                    printf("Voici le perroquet numero %d : %s",compteur,chaine);
                    compteur++;
                }
                else
                {
                    printf("\n");
                }
                com=0;
            }
            else
            {
                if(com==0)
                {
                    com=1;
                    printf("Voici le perroquet numero %d : %s",compteur,chaine);
                    compteur++;
                }
                else
                {
                    printf("%s",chaine);
                }
            }
        }
        fflush(stdin);
        gets(perroquet);
        fputs(perroquet,f_pero);
        fputs("\n",f_pero);
        fclose(f_pero);
    }
}


//Fonction pour le choix d'encrypter ou decrypter
void SaisiCryptage(char* crypt)
{
    while(crypt[0]==NULL)
    {
        printf("Voulez vous encrypte ou decrypte (e/d) ?\n");
        fflush(stdin);
        gets(crypt);
        if(crypt[0]!='e' && crypt[0]!='d')
        {
            crypt[0]=NULL;
        }
    }
}
