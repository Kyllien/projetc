#include <stdio.h>
#include <stdlib.h>

#include "entete.h"

//Encrypte un char
char Encryptage(char x, char y)
{
    char rep;
    rep=(char)((int)x-(int)y);
    return rep;
}

//Decrypte un char
char Decryptage(char x, char y)
{
    char rep;
    rep=(char)((int)x+(int)y);
    return rep;
}


void Cryptage(char *nom,char *perroquet, char *crypt)
{
    //Declaration
    char chaine[TAILLE] = "";
    int c1=0;
    FILE* f_source=NULL;
    FILE* f_dest=NULL;


    f_source = fopen(nom,"r");
    f_dest = fopen("dest.crt","w+");



    if (f_source != NULL && f_dest != NULL )
    {
        while(fgets(chaine, TAILLE, f_source) != NULL)
        {
            char s_crypte[TAILLE]= "";
            for(int i = 0; i<=TAILLE;i++)
            {
                if(chaine[i]!=NULL)
                {
                    printf("%c + %c : %d = %c\n",perroquet[c1],chaine[i],(int)chaine[i],(int)perroquet[c1]-
                           (int)chaine[i],Encryptage(chaine[i],perroquet[c1]));

                    if(crypt[0]=='e')
                    {
                        s_crypte[i]=Encryptage(chaine[i],perroquet[c1]);
                    }
                    else
                    {
                        s_crypte[i]=Decryptage(chaine[i],perroquet[c1]);
                    }

                    chaine[i]=NULL;

                    //Pour compteur du perroquet
                    if(c1==strlen(perroquet)-1)
                    {
                        c1=0;
                    }
                    else
                    {
                        c1++;
                    }
                }
            }
            fputs(s_crypte,f_dest);
        }

        //Suppression du fichier source$
        fclose(f_source);
        if (remove(nom) == 0){
            printf(" Le fichier %s a ete supprime avec succes.\n",nom);
        }
        else
        {
            printf("Impossible de supprimer le fichier\n");
        }


        fclose(f_dest);
    }
}
